# storage/nvdimm/btt_devdax_fsdax_raw_switch_simultaneously

Storage: nvdimm btt devdax fsdax raw switch simultaneously test BZ1472034

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
