# storage/ssd/test_devs_setup 

Storage: nvme ssd TEST_DEVS setup 

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
#### You can define test devs by seting parameter TEST_DEVS
```bash
bash ./runtest.sh
```
